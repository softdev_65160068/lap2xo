/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lapxo;

/**
 *
 * @author USER
 */
public class LapXo {
    static char[][]table = {{'-','-','-'},
                            {'-','-','-'},
                            {'-','-','-'}};
    static char Currentplayer ='X';
    static void PrintWelcome(){
        System.out.println("Welcome to TIC TAC TOE Game");
    }

    static void Table(){
       for(int i=0;i<3;i++){
           for(int j=0;j<3;j++){
               System.out.print(table[i][j]+" ");
           }
           System.out.println();
       }
    }
    static void Turn(){
        System.out.println(Currentplayer+" Turn");
    }

    public static void main(String[] args) {
        PrintWelcome();
        Table();
        Turn();
    }
}
